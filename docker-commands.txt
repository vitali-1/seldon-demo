# Start client
docker build -t deep-mnist-client && docker run--name deep-mnist-client -p 80:80 deep-mnist-client
docker rm deep-mnist-client --force

# Start and remove nodejs_mnist_predictor
docker run --name "nodejs_mnist_predictor" -d --rm -p 5000:5000 node-s2i-mnist-model:0.1
docker rm nodejs_mnist_predictor --force

# Start R container
docker run --name "r-mnist_predictor" -d --rm -p 5000:5000 r-mnist:0.2
!docker rm r-mnist_predictor --force


# Show as asked Joy deep_ mnist - http://localhost:8888/notebooks/Downloads/seldon-core/examples/models/deep_mnist/deep_mnist.ipynb